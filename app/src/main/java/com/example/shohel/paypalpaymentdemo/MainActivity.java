package com.example.shohel.paypalpaymentdemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private List<ProductDM> productDMs;
    private ListView paypalList;
    private static final int REQUEST_CODE_PAYMENT = 1;
    // PayPal configuration
    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)

            .clientId("AQyX2CLvhLVu3Bgoj3zYhU0mmfmx27bjv59GemxOU93tCOECsXJnGIiiIwMhGmkg8yDLTUE8BHk-M0rp");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        initUI();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void initUI() {
        paypalList=(ListView)findViewById(R.id.paypalList);
        productDMs = new ArrayList<>();
        if (productDMs != null) {
            productDMs.clear();
        }
        ProductDM productDM = new ProductDM();
        productDM.setName("Shirt");
        productDM.setPrice("10");
        productDMs.add(productDM);

        ProductDM productDM1 = new ProductDM();
        productDM1.setName("Pant");
        productDM1.setPrice("20");
        productDMs.add(productDM1);

        ProductDM productDM2 = new ProductDM();
        productDM2.setName("T-shirt");
        productDM2.setPrice("30");
        productDMs.add(productDM2);
        paypalList.setAdapter(new IslandListAdapter());

    }

    private class IslandListAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView productName,price;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return productDMs.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return productDMs.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final ViewHolder holder;
            if(convertView == null){
                convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_item, parent, false);

                holder = new ViewHolder();
                holder.productName = (TextView)convertView.findViewById(R.id.productName);
                holder.price=(TextView)convertView.findViewById(R.id.price);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }
            holder.productName.setText(productDMs.get(position).getName());
            holder.price.setText(productDMs.get(position).getPrice());
            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ProductDM  productDM= productDMs.get(position);
                    onBuyPressed(productDM);
                }
            });

            return convertView;
        }

    }
    public void onBuyPressed(ProductDM productDM) {

        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalPayment payment = new PayPalPayment(new BigDecimal(productDM.getPrice()), "USD", productDM.getName(),
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }
}
